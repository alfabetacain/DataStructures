public class ResizingArray<Item>
{
	private Item[] array;
	private int cursor;

 public ResizingArray()
 {
	 array = (Item[]) new Object[2];
	 cursor = 0;
 }
 public static void main(String[] args)
 {
	
 }

 public void add(Item item)
 {
	if (cursor == array.length) upsize();
	array[cursor++] = item;	
 }
 
 public Item get(int i)
 {
	return array[i];
 }

 public Item delete(int i)
 {
	Item deletion = array[i];
	for (int j = i+1; j <= cursor; j++)
	{
		array[j-1] = array[j];	
	}
	cursor--;
	if (cursor < array.length / 4) downsize();
	return deletion;
 }

 private void downsize()
 {
	Item[] newArray = (Item[]) new Object[array.length/2];
	for (int i = 0; i < cursor; i++)
	{
		newArray[i] = array[i];
	}
	array = newArray;
 }

 private void upsize()
 {
	Item[] newArray = (Item[]) new Object[array.length*2];
	for (int i = 0; i < cursor; i++)
	{
		newArray[i] = array[i];
	}
	array = newArray;
 }
}
