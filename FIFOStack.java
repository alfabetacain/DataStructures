public class FIFOStack<Item>
{
  private Item[] array;
  private int start;
  private int end;

  public FIFOStack()
  {
	this(2);
  }

  public FIFOStack(int size)
  {
    array = (Item[]) new Object[size];
    start = end = 0;
  }

  public void put(Item item)
  {
    if (end == start)
	    upsize();
    array[end++ % array.length] = item;
  }

  public void upsize()
  {
    Item[] newArray = (Item[]) new Object[array.length*2];
    for (int i = 0; i + start < end; i++)
    {
      newArray[i] = array[i+start % array.length];
    }
      end = end - start;
      start = 0;
      array = newArray;
  }

  public Item pop()
  {
    Item deletion = array[start++ % array.length];
    if (Math.abs(end - start) < array.length / 4)
	    downsize();
    return deletion;
  }

  public void downsize()
  {
    Item[] newArray = (Item[]) new Object[array.length/2];
    for (int i = 0; i + start < end; i++)
    {
      newArray[i] = array[i+start % array.length];
    }
      end = end - start;
      start = 0;
      array = newArray;
  }
}
